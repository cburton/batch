#!/usr/bin/env python
import sys
import ROOT
import pickle
import array

def storeData():
	skimFile = ROOT.TFile.Open('skimmed.root')
	skimTree = skimFile.Get('nominal')

	skimTree.SetBranchStatus('*', 0)
	for activeBranch in ['transverseMassW', 'electronEvent', 'electron_pt', 'muon_pt']:
		skimTree.SetBranchStatus(activeBranch, 1)

	outFile = ROOT.TFile.Open('output.root', 'RECREATE')
	outTree = skimTree.CloneTree()

	outFile.Write()

	return True

def storeMc():
	skimFile = ROOT.TFile.Open('skimmed.root')
	skimTree = skimFile.Get('nominal')
	total = 0
	w = -1
	for event in skimTree:
		if event.totalEventsWeighted != w:
			w = event.totalEventsWeighted
			total += w

	skimTree.SetBranchStatus('*', 0)
	for activeBranch in ['transverseMassW', 'mcWeight*', 'electronEvent', 'electron_pt', 'muon_pt']:
		skimTree.SetBranchStatus(activeBranch, 1)

	outFile = ROOT.TFile.Open('output.root', 'RECREATE')
	outTree = skimTree.CloneTree()

	sumTree = ROOT.TTree('sumWeights', 'sumWeights')
	sumBranch = array.array('f', [total])
	sumTree.Branch('totalEventsWeighted', sumBranch, 'totalEventsWeighted/F')
	sumTree.Fill()

	outFile.Write()

	return True

def store(isData):
	if isData:
		return storeData()
	else:
		return storeMc()

if __name__ == '__main__':
	isData = sys.argv[1]=='True'
	success = False
	print('Transforming file.')
	success = store(isData)
	assert success, 'Failed to store file.'