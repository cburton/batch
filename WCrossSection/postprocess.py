#!/usr/bin/env python
import pickle
import glob

_INDIR = '/data/cburton/WCrossSection/out/'
_OUTDIR = '/data/cburton/WCrossSection/merge/'

sections = ['TOTALW', 'TOTALW-B', 'TOTALW-C', 'TOTALW-D', 'TOTALW-E', 'TOTALW-F']
dids = [
    # 361100, 361101, 361102,
    # 361103, 361104, 361105,
    # 361106, 361107, 361108,
    # 363355, 363356, 363357, 363358, 363359, 363360, 363489, 363494, 364250, 364253, 364254, 364255, 364288, 364289, 364290,
    # 410470, 410644, 410645, 410646, 410647, 410654, 410655, 410658, 410659,
    # 364100, 364101, 364102, 364103, 364104, 364105, 364106, 364107, 364108, 364109, 364110, 364111, 364112, 364113,
    # 364114, 364115, 364116, 364117, 364118, 364119, 364120, 364121, 364122, 364123, 364124, 364125, 364126, 364127,
    # 364128, 364129, 364130, 364131, 364132, 364133, 364134, 364135, 364136, 364137, 364138, 364139, 364140, 364141,
    # 364156, 364157, 364158, 364159, 364160, 364161, 364162, 364163, 364164, 364165, 364166, 364167, 364168, 364169,
    # 364170, 364171, 364172, 364173, 364174, 364175, 364176, 364177, 364178, 364179, 364180, 364181, 364182, 364183,
    # 364184, 364185, 364186, 364187, 364188, 364189, 364190, 364191, 364192, 364193, 364194, 364195, 364196, 364197,
]
mc_profiles = ['_a', '_d', '_e']

for section in sections:
    print(f'Working on section {section}.')
    for did in dids:
        print(f'  Working on did {did}.')
        for profile in mc_profiles:
            print(f'    Working on profile mc16{profile[1]}.')
            totalWeights = 0
            mtwList = []
            weightList = []
            channelList = []
            inputFiles = glob.glob(f'{_INDIR}/{did}{profile}/{section}/*.pickle')
            for inputFile in inputFiles:
                with open(inputFile, 'rb') as f:
                    totalWeights += pickle.load(f)
                    mtwList.extend(pickle.load(f))
                    weightList.extend(pickle.load(f))
                    channelList.extend(pickle.load(f))
            with open(f'{_OUTDIR}/{did}{profile}.{section}.pickle', 'wb') as f:
                pickle.dump(totalWeights, f)
                pickle.dump(mtwList, f)
                pickle.dump(weightList, f)
                pickle.dump(channelList, f)