#!/usr/bin/env bash
hostname
echo $@
pwd

# Step 1: setup input files as output.root
python3 setup.py ${@:4}

# Step 2: run selections ntupler
tar xzf selections.tar.gz
python3 transform.py $@

# Step 3: store sumWeight/MTW/weight to pickle file
python3 store.py $1