#!/usr/bin/env bash
hostname
echo $@
pwd
ls /cvmfs
ls /cvmfs/atlas.cern.ch

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet
source $AtlasSetup/scripts/asetup.sh Athena,master,latest

# Step 1: setup input files as output.root
source ${ATLAS_LOCAL_ROOT_BASE}/utilities/oldAliasSetup.sh rucio
python setup.py ${@:4}

# Step 2: run selections ntupler
tar xzf selections.tar.gz
python transform.py $@

# Step 3: store sumWeight/MTW/weight to pickle file
python store.py $1