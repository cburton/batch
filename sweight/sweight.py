import ROOT
import ROOT.RooFit as RRF

import os, sys, time
from glob import glob
import random
random.seed(0)

nbins_m, mlim = 100, [2.7, 3.5]
nbins_t, tlim = 100, [-1., 9.]
nbins_mtw, mtwlim = 10, [0, 200]
nbins_dp, dplim = 10, [0, 3.1416]
nbins_dy, dylim = 10, [0, 4.6]
nbins_z0, z0lim = 10, [0, 40]

_MAXTOTALFILES = 60
_MAXTOTALEVENTS = 100_000

_INPUTTOTAL = '/data/cburton/sweight/BPHY1_ntuples/*/user.cburton.*.output.root'
_INPUTASSOC = '*.data.root'
_INPUTASSOCS = [
    '/data/cburton/ntuples/ABCD/ABCD-A.data.root',
    '/data/cburton/ntuples/ABCD/ABCD-B.data.root',
    '/data/cburton/ntuples/ABCD/ABCD-C.data.root',
    '/data/cburton/ntuples/ABCD/ABCD-D.data.root',
    '/data/cburton/ntuples/ABCD/ABCD-E.data.root',
    '/data/cburton/ntuples/ABCD/ABCD-F.data.root',
]
_YS = [[0, 1], [1, 2.1]]
_PTS = [[8, 10], [10, 14], [14, 18], [18, 30], [30, 60], [60, 200]]

def setupWorkspace(w, p=None):
    # Mass --- signal
    w.factory(f'Jpsi_m[{mlim[0]}, {mlim[1]}]')
    w.factory(f'm_mean[{p["m_mean"] if p else "3.097, 3.087, 3.107"}]')
    w.factory(f'm_sigma[{p["m_sigma"] if p else "0.04, 0.03, 0.07"}]')
    w.factory(f'm_sigma2[{p["m_sigma2"] if p else "0.1, 0.05, 0.15"}]')
    w.factory(f'm_f[{p["m_f"] if p else "0.6, 0, 1"}]')
    w.factory('Gaussian::Mass_Gauss1(Jpsi_m, m_mean, m_sigma)')
    w.factory('Gaussian::Mass_Gauss2(Jpsi_m, m_mean, m_sigma2)')
    w.factory(f'SUM::Mass_Jpsi(m_f * Mass_Gauss1, Mass_Gauss2)')

    # Mass --- backgrounds
    w.factory(f'm_slopePrompt[{p["m_slopePrompt"] if p else "0.6, 0, 1"}]')
    w.factory('expr::m_slopePrompt2("1-m_slopePrompt", m_slopePrompt)')
    w.factory('Bernstein::Mass_CombPrompt(Jpsi_m, {m_slopePrompt, m_slopePrompt2})')

    w.factory(f'm_slopeNonPrompt1[{p["m_slopeNonPrompt1"] if p else "0.8, 0, 1"}]')
    w.factory('expr::m_complementNonPrompt1("1-m_slopeNonPrompt1", m_slopeNonPrompt1)')
    w.factory('Bernstein::Mass_CombNonPrompt1(Jpsi_m, {m_slopeNonPrompt1, m_complementNonPrompt1})')

    w.factory(f'm_slopeNonPrompt2[{p["m_slopeNonPrompt2"] if p else "0.5, 0, 1"}]')
    w.factory('expr::m_complementNonPrompt2("1-m_slopeNonPrompt2", m_slopeNonPrompt2)')
    w.factory('Bernstein::Mass_CombNonPrompt2(Jpsi_m, {m_slopeNonPrompt2, m_complementNonPrompt2})')

    # Tau --- resolution/prompt
    w.factory(f'Jpsi_tau[{tlim[0]}, {tlim[1]}]')
    w.factory(f't_sigma[{p["t_sigma"] if p else "0.05, 0.01, 0.1"}]')
    # w.factory(f't_sigma[{p["t_sigma"] if p else "0.05"}, 0.01, 0.1]')
    w.factory('GaussModel::Tau_Resolution1(Jpsi_tau, t_mean[0], t_sigma)')
    w.factory('expr::t_sigma2("2*t_sigma", t_sigma)')
    w.factory(f'GaussModel::Tau_Resolution2(Jpsi_tau, t_mean, t_sigma2)')
    w.factory(f't_fRes[{p["t_fRes"] if p else "0.5, 0, 1"}]')
    # w.factory(f't_fRes[{p["t_fRes"] if p else "0.5"}, 0, 1]')
    w.factory(r'AddModel::Tau_Resolution({Tau_Resolution1, Tau_Resolution2}, {t_fRes})')

    # Tau --- non-prompt
    w.factory(f't_lifetimeNonPromptJpsi[{p["t_lifetimeNonPromptJpsi"] if p else "1.3, 1.2, 1.6"}]')
    # w.factory(f't_lifetimeNonPromptJpsi[{p["t_lifetimeNonPromptJpsi"] if p else "1.3"}, 1.2, 1.6]')
    w.factory('Decay::Tau_NonPromptJpsi(Jpsi_tau, t_lifetimeNonPromptJpsi, Tau_Resolution, SingleSided)')

    w.factory(f't_lifetimeNonPromptComb1[{p["t_lifetimeNonPromptComb1"] if p else "1.4, 1.2, 1.6"}]')
    # w.factory(f't_lifetimeNonPromptComb1[{p["t_lifetimeNonPromptComb1"] if p else "1.4"}, 1.2, 1.6]')
    w.factory('Decay::Tau_NonPromptComb1(Jpsi_tau, t_lifetimeNonPromptComb1, Tau_Resolution, SingleSided)')

    w.factory(f't_lifetimeNonPromptComb2[{p["t_lifetimeNonPromptComb2"] if p else "0.2, 0.1, 0.3"}]')
    # w.factory(f't_lifetimeNonPromptComb2[{p["t_lifetimeNonPromptComb2"] if p else "0.2"}, 0.1, 0.3]')
    w.factory('Decay::Tau_NonPromptComb2(Jpsi_tau, t_lifetimeNonPromptComb2, Tau_Resolution, DoubleSided)')

    # Joint probabilities
    w.factory('PROD::MassTau_NC1(Mass_CombNonPrompt1, Tau_NonPromptComb1)')
    w.factory('PROD::MassTau_NC2(Mass_CombNonPrompt2, Tau_NonPromptComb2)')
    w.factory(f'SUM::MassTau_NC(j_f[{p["j_f"] if p else "0.7, 0, 1"}] * MassTau_NC1, MassTau_NC2)')
    w.factory('PROD::MassTau_NJ(Mass_Jpsi, Tau_NonPromptJpsi)')
    w.factory('PROD::MassTau_PC(Mass_CombPrompt, Tau_Resolution)')
    w.factory('PROD::MassTau_PJ(Mass_Jpsi, Tau_Resolution)')

    # Sum
    w.factory('SUM::MassTauPDF(n_NC[2e4, 0, 1e7] * MassTau_NC, \
                               n_NJ[2e4, 0, 1e7] * MassTau_NJ, \
                               n_PC[5e2, 0, 1e7] * MassTau_PC, \
                               n_PJ[4e4, 0, 1e7] * MassTau_PJ)')

    # Add s-weight variables for associated workspace
    if p:
        w.factory(f'transverseMassW[{mtwlim[0]}, {mtwlim[1]}]')
        w.factory(f'deltaPhiJpsiW[{dplim[0]}, {dplim[1]}]')
        w.factory(f'deltaPhiJpsiLepton[{dplim[0]}, {dplim[1]}]')
        w.factory(f'deltaYJpsiW[{dylim[0]}, {dylim[1]}]')
        w.factory(f'deltaYJpsiLepton[{dylim[0]}, {dylim[1]}]')
        w.factory(f'deltaz0[{z0lim[0]}, {z0lim[1]}]')

def parameterContainers(workspace, interest=None, nuisance=None, valuesOnly=False):
    interestDict,nuisanceDict = {}, {}
    variableNames = workspace.allVars().contentsString().split(',')
    for variableName in variableNames:
        variable = workspace.var(variableName)
        if valuesOnly:
            variable = variable.getVal()
        explicitInterest = interest and interest in variableName
        explicitNuisance = nuisance and nuisance in variableName
        defaultInterest = not interest and nuisance and nuisance not in variableName
        defaultNuisance = not nuisance and interest and interest not in variableName
        if explicitInterest or defaultInterest:
            interestDict[variableName] = variable
        elif explicitNuisance or defaultNuisance:
            nuisanceDict[variableName] = variable
        else:
            print(f'Unsure if {variableName} is interest or nuisance parameter.')
    return interestDict, nuisanceDict

def writeParameters(fileName, *args):
    with open(fileName, 'w') as paramFile:
        currentTime = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
        paramFile.write(currentTime)
        for paramDict in args:
            paramFile.write('\n')
            for key,value in paramDict.items():
                if isinstance(value, ROOT.RooRealVar): value = value.getVal()
                paramFile.write(f'{key}, {value}\n')

def makeFitHistograms(workspace, dataset, pdf, fit, tag=''):
    # 2D mass-tau histograms.
    Jpsi_m = workspace.var('Jpsi_m')
    Jpsi_tau = workspace.var('Jpsi_tau')
    h_data, h_fit = binHist(dataset, pdf, [Jpsi_m, Jpsi_tau])
    h_pulls = pullsHist(h_data, h_fit)
    h_fraction = fractionHist(h_data, h_fit)
    # 2D data.
    h_data.Draw('colz')
    h_data.Write(f'data.{tag}')
    # 2D fit.
    h_fit.Draw('colz')
    h_fit.Write(f'fit.{tag}')
    # 2D pulls.
    h_pulls.Draw('colz')
    h_pulls.Write(f'pulls.{tag}')
    # 2D fractions.
    h_fraction.Draw('colz')
    h_fraction.Write(f'fraction.{tag}')
    # Mass projection.
    massFrame = Jpsi_m.frame()
    dataset.plotOn(massFrame)
    pdf.plotOn(massFrame)
    massFrame.Draw()
    massFrame.Write(f'projection.mass.{tag}')
    # Tau projection.
    tauFrame = Jpsi_tau.frame()
    dataset.plotOn(tauFrame)
    pdf.plotOn(tauFrame)
    tauFrame.Draw()
    tauFrame.Write(f'projection.tau.{tag}')
    # Correlation matrix.
    corrHist = fit.correlationHist()
    corrHist.Draw('colz')
    corrHist.Write(f'correlation.{tag}')

def binHist(dataset, pdf, variables, smooth=1):
    h_data = dataset.createHistogram(variables[0], variables[1], nbins_m, nbins_t)
    h_fit = pdf.createHistogram('Jpsi_m,Jpsi_tau', nbins_m*smooth, nbins_t*smooth)
    scale = h_data.Integral() / h_fit.Integral() * smooth
    h_fit.Scale(scale)
    return h_data, h_fit

def pullsHist(dataHist, fitHist):
    pulls = ROOT.TH2F('pulls', '', nbins_m, mlim[0], mlim[1], nbins_t, tlim[0], tlim[1])
    for binNumber in range(nbins_m * nbins_t):
        dataBinErr = dataHist.GetBinError(binNumber)
        if dataBinErr==0: continue
        pdfBinVal = fitHist.GetBinContent(binNumber)
        dataBinVal = dataHist.GetBinContent(binNumber)
        pulls.SetBinContent(binNumber, (dataBinVal - pdfBinVal) / dataBinErr)
    return pulls

def fractionHist(dataHist, fitHist):
    fraction = ROOT.TH2F('fraction', '', nbins_m, mlim[0], mlim[1], nbins_t, tlim[0], tlim[1])
    for binNumber in range(nbins_m * nbins_t):
        pdfBinVal = fitHist.GetBinContent(binNumber)
        if pdfBinVal==0: continue
        dataBinVal = dataHist.GetBinContent(binNumber)
        fraction.SetBinContent(binNumber, (dataBinVal - pdfBinVal) / pdfBinVal)
    return fraction

def makeSWeightHistograms(variablePacks, datasetDict, tag=''):
    for variable, bins, limits in variablePacks:
        variableName = variable.GetName()
        for yieldName, sDataset in datasetDict.items():
            # Create histogram for this variable and region
            histArgs = (sDataset, '', variable, RRF.Binning(bins, *limits))
            histogram = ROOT.RooAbsData.createHistogram(*histArgs)
            histogramKey = variableName + '.' + yieldName.split('_')[2]
            # Save the binned histogram
            histogram.Draw()
            histogram.SetName(f'{histogramKey}.{tag}')
            histogram.Write()

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        import errno
        if exc.errno == errno.EEXIST and os.path.isdir(path): pass
        else: raise

def fitOneBin(yBin, ptBin):
    print(f'S-weighting for y={yBin} and pt={ptBin}.')
    binKey = f'pt{ptBin[0]}.y{yBin[0]}'
    outputRootFile = ROOT.TFile.Open(f'sVars.root', 'recreate')
    outputTextFile = f'fits.txt'

    # Define variables for total and associated datasets
    Jpsi_m = ROOT.RooRealVar('Jpsi_m', '', mlim[0], mlim[1])
    Jpsi_tau = ROOT.RooRealVar('Jpsi_tau', '', tlim[0], tlim[1])
    Jpsi_pt = ROOT.RooRealVar('Jpsi_pt', '', 8)
    Jpsi_y = ROOT.RooRealVar('Jpsi_y', '', 0)
    JpsiVars = [Jpsi_pt, Jpsi_y, Jpsi_m, Jpsi_tau]

    Jpsi_mu0_pt = ROOT.RooRealVar('Jpsi_mu0_pt', '', 0)
    Jpsi_mu1_pt = ROOT.RooRealVar('Jpsi_mu1_pt', '', 0)
    Jpsi_mu0_eta = ROOT.RooRealVar('Jpsi_mu0_eta', '', 0)
    Jpsi_mu1_eta = ROOT.RooRealVar('Jpsi_mu1_eta', '', 0)
    runYear = ROOT.RooRealVar('runYear', '', 2015, 2018)
    channel = ROOT.RooCategory('channel', '')
    channel.defineType('Electron', 0); channel.defineType('Muon', 1)
    efficiencyVars = [Jpsi_mu0_pt, Jpsi_mu1_pt, Jpsi_mu0_eta, Jpsi_mu1_eta, runYear, channel]

    # Define s-weighted variables for associated dataset
    transverseMassW = ROOT.RooRealVar('transverseMassW', '', mtwlim[0], mtwlim[1])
    deltaPhiJpsiW = ROOT.RooRealVar('deltaPhiJpsiW', '', dplim[0], dplim[1])
    deltaPhiJpsiLepton = ROOT.RooRealVar('deltaPhiJpsiLepton', '', dplim[0], dplim[1])
    deltaYJpsiW = ROOT.RooRealVar('deltaYJpsiW', '', dylim[0], dylim[1])
    deltaYJpsiLepton = ROOT.RooRealVar('deltaYJpsiLepton', '', dylim[0], dylim[1])
    deltaz0 = ROOT.RooRealVar('deltaz0', '', z0lim[0], z0lim[1])
    sWeightVars = [
        deltaPhiJpsiW, deltaPhiJpsiLepton,
        deltaYJpsiW, deltaYJpsiLepton,
        transverseMassW, deltaz0,
    ]

    # ====== TOTAL DATA ======
    # Load total J/psi data into TTree
    print('Loading total J/psi data. The following files will be used:')
    totalFileNames = glob(_INPUTTOTAL)
    random.shuffle(totalFileNames)
    totalFileNames = totalFileNames[:_MAXTOTALFILES]
    [print('    ' + _) for _ in totalFileNames]
    totalNominal = ROOT.TChain('nominal')
    for totalFileName in totalFileNames:
        totalNominal.Add(totalFileName)
    print(f'Total tree created with {totalNominal.GetEntries()} J/psi events.')

    # Import total dataset into RooDataSet
    totalArgSet = ROOT.RooArgSet(*JpsiVars)
    totalDataset = ROOT.RooDataSet('totalDataset', 'totalDataset', totalArgSet)
    totalEvents = 0
    for event in totalNominal:
        ptVal = event.Jpsi_pt * 1e-3
        if not ptBin[0] < ptVal < ptBin[1]: continue
        yVal = abs(event.Jpsi_y)
        if not yBin[0] < yVal < yBin[1]: continue
        mVal = event.Jpsi_m * 1e-3
        if not mlim[0] < mVal < mlim[1]: continue
        tVal = event.Jpsi_tau
        if not tlim[0] < tVal < tlim[1]: continue
        Jpsi_pt.setVal(ptVal)
        Jpsi_y.setVal(yVal)
        Jpsi_m.setVal(mVal)
        Jpsi_tau.setVal(tVal)
        totalDataset.add(totalArgSet)
        totalEvents += 1
        if totalEvents >= _MAXTOTALEVENTS:
            break
    print(f'Total dataset created with {totalDataset.sumEntries()} J/psi events.')

    # ====== ASSOCIATED DATA ======
    # Load associated J/psi+W data into TTree
    print('Loading associated J/psi data. The following files will be used:')
    assocFileNames = glob(_INPUTASSOC)
    [print('    ' + _) for _ in assocFileNames]
    assocNominal = ROOT.TChain('nominal')
    for assocFileName in assocFileNames:
        assocNominal.Add(assocFileName)
    print(f'Associated tree created with {assocNominal.GetEntries()} J/psi+W events.')

    # Import associated dataset into RooDataSet
    assocArgSet = ROOT.RooArgSet()
    [assocArgSet.add(_) for _ in JpsiVars + efficiencyVars + sWeightVars]
    assocDataset = ROOT.RooDataSet('assocDataset', 'assocDataset', assocArgSet)
    for event in assocNominal:
        ptVal = event.Jpsi_pt[event.Jpsi_selected] * 1e-3
        if not ptBin[0] < ptVal < ptBin[1]: continue
        yVal = abs(event.Jpsi_y[event.Jpsi_selected])
        if not yBin[0] < yVal < yBin[1]: continue
        Jpsi_pt.setVal(ptVal)
        Jpsi_y.setVal(yVal)
        Jpsi_m.setVal(event.Jpsi_m[event.Jpsi_selected] * 1e-3)
        Jpsi_tau.setVal(event.Jpsi_tau[event.Jpsi_selected])

        Jpsi_mu0_pt.setVal(event.muon_pt[event.Jpsi_muons[event.Jpsi_selected][0]] * 1e-3)
        Jpsi_mu1_pt.setVal(event.muon_pt[event.Jpsi_muons[event.Jpsi_selected][1]] * 1e-3)
        Jpsi_mu0_eta.setVal(event.muon_eta[event.Jpsi_muons[event.Jpsi_selected][0]])
        Jpsi_mu1_eta.setVal(event.muon_eta[event.Jpsi_muons[event.Jpsi_selected][1]])
        runYear.setVal(event.runYear)
        channel.setIndex(event.muonEvent) # 0 = electron event, 1 = muon event

        deltaPhiJpsiW.setVal(event.deltaPhiJpsiW)
        deltaPhiJpsiLepton.setVal(event.deltaPhiJpsiLepton)
        deltaYJpsiW.setVal(event.deltaYJpsiW)
        deltaYJpsiLepton.setVal(event.deltaYJpsiLepton)
        transverseMassW.setVal(event.transverseMassW)
        deltaz0.setVal(event.Jpsi_deltaz0)
        assocDataset.add(assocArgSet)
    print(f'Associated dataset created with {assocDataset.sumEntries()} J/psi+W events.')

    # ====== TOTAL FIT ======
    # Setup RooWorkspace. Define total fit parameters and PDFs.
    print('Setting up total J/psi workspace.')
    totalWorkspace = ROOT.RooWorkspace('totalWorkspace')
    setupWorkspace(totalWorkspace)
    totalMassTauPDF = totalWorkspace.pdf('MassTauPDF')

    # Fit PDF to total dataset.
    print('Fitting to the full data.')
    totalFit = totalMassTauPDF.fitTo(totalDataset, RRF.Save())

    # Store fit parameter values in dictionaries.
    totalYields, totalNuisances = parameterContainers(totalWorkspace, interest='n_', valuesOnly=True)

    print('Making histograms from total J/psi fit.')
    makeFitHistograms(totalWorkspace, totalDataset, totalMassTauPDF, totalFit, f'total.{binKey}')

    # ====== ASSOCIATED FIT ======
    # Setup RooWorkspace. TotalDefine associated fit parameters and PDFs.
    assocWorkspace = ROOT.RooWorkspace('assocWorkspace')
    setupWorkspace(assocWorkspace, totalNuisances)
    assocMassTauPDF = assocWorkspace.pdf('MassTauPDF')

    # Fit PDF to associated dataset.
    print('Fitting to associated J/psi model.')
    assocFit = assocMassTauPDF.fitTo(assocDataset, RRF.Save())
    # Store fit parameters in dictionaries.
    assocYields, assocNuisances = parameterContainers(assocWorkspace, interest='n_')

    print('Making histograms for associated J/psi fit.')
    makeFitHistograms(assocWorkspace, assocDataset, assocMassTauPDF, assocFit, f'assoc.{binKey}')

    # Write parameters from fit to output file
    writeParameters(outputTextFile, totalYields, totalNuisances, assocYields, assocNuisances)

    # === S-WEIGHTING ===
    print('Perform s-weighting.')
    # Create a RooArgList of yield parameters
    yieldsList = ROOT.RooArgList(*assocYields.values())
    # S-weight the data
    [var.setConstant() for var in assocNuisances.values()]
    sPlot = ROOT.RooStats.SPlot('MassTau_sData', '', assocDataset, assocMassTauPDF, yieldsList)
    [var.setConstant(ROOT.kFALSE) for var in assocNuisances.values()]
    # Create s-weighted datasets
    sDatasetDict = {}
    for yieldName in assocYields.keys():
        sDatasetName = f'sDataset_{yieldName}_{binKey}'
        assocDatasetWeighted = ROOT.RooDataSet(
            sDatasetName, '',
            assocDataset, assocDataset.get(),
            '', f'{yieldName}_sw',
        )
        sDatasetDict[sDatasetName] = assocDatasetWeighted

    # === PLOT S-WEIGHTED VARIABLES ===
    pack = [
        (deltaPhiJpsiW, nbins_dp, dplim),
        (deltaPhiJpsiLepton, nbins_dp, dplim),
        (deltaYJpsiW, nbins_dy, dylim),
        (deltaYJpsiLepton, nbins_dy, dylim),
        (transverseMassW, nbins_mtw, mtwlim),
        (deltaz0, nbins_z0, z0lim),
    ]
    makeSWeightHistograms(pack, sDatasetDict, binKey)
    # Save tree of sweights to the output file
    assocDatasetWeighted.convertToTreeStore()
    assocDatasetWeighted.SetName(f'sWeightTree.pt{ptBin[0]}.y{yBin[0]}')
    assocDatasetWeighted.tree().Write()
    outputRootFile.Close()

if __name__=='__main__':
    i = int(sys.argv[1])
    inputAssoc = _INPUTASSOCS[i // (len(_YS) * len(_PTS))]
    os.popen(f'cp {inputAssoc} .')
    yBin = _YS[(i // len(_PTS)) % len(_YS)]
    ptBin = _PTS[i % len(_PTS)]
    fitOneBin(yBin, ptBin)
    print('Done')