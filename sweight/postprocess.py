#!/usr/bin/env python
from glob import glob
from subprocess import run

_DEBUG = False

_INPUTDIR = './out/'
_OUTPUTDIR = './out/'

_REGIONS = 'ABCDEF'
_YS = [0, 1]
_PTS = [8, 10, 14, 18, 30, 60]

i = 0
commands = []
for region in _REGIONS:
	for y in _YS:
		for pt in _PTS:

			# rename fit file
			oldFitFileName = glob(f'{_INPUTDIR}/fits.*._{i}.txt')
			if len(oldFitFileName)!=1: break
			newFitFileName = f'{_OUTPUTDIR}/fits.{region}.pt{pt}.y{y}.txt'
			commands.append(['mv', oldFitFileName[0], newFitFileName])

			# rename root file
			oldRootFileName = glob(f'{_INPUTDIR}/sVars.*._{i}.root')
			if len(oldRootFileName)!=1: break
			newRootFileName = f'{_OUTPUTDIR}/sVars.{region}.pt{pt}.y{y}.root'
			commands.append(['mv', oldRootFileName[0], newRootFileName])

			i += 1

if i==(len(_REGIONS) * len(_YS) * len(_PTS)):
	for command in commands:
		if _DEBUG:
			print(command)
		else:
			run(command)