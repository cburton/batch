#!/usr/bin/env python
import os, sys
from array import array

import ROOT
ROOT.xAOD.Init().isSuccess()

from inputs import *
filename = files_801130[int(sys.argv[1])] # DPS 63

def checkStatus(particle, stati=[1]):
    return particle.status() in stati

def inParents(particle, parentPDG, allowAntiParticle=True):
    nP = particle.nParents()
    for parent_index in range(nP):
        if allowAntiParticle:
            pPDG = particle.parent(parent_index).absPdgId()
            parentPDG = abs(parentPDG)
        else:
            pPDG = particle.parent(parent_index).pdgId()
        if pPDG == parentPDG:
            return True
    return False

def inChildren(particle, childPDG, allowAntiParticle=True):
    nC = particle.nChildren()
    for child_index in range(nC):
        try:
            if allowAntiParticle:
                cPDG = particle.child(child_index).absPdgId()
                childPDG = abs(childPDG)
            else:
                cPDG = particle.child(child_index).pdgId()
            if cPDG == childPDG:
                return True
        except ReferenceError as e:
            pass
    return False

def checkChain(particle, pt=0., first=False, last=True, allowAntiParticle=False):
    # only take the first particle in a chain
    if first and inParents(particle, particle.pdgId(), allowAntiParticle):
        return False
    # only take the last partice in a chain
    if last and inChildren(particle, particle.pdgId(), allowAntiParticle):
        return False
    # check pT requirement if desired
    if particle.pt() < pt:
        return False
    return True

outputFile = ROOT.TFile.Open('output.root', 'recreate')
h = ROOT.TH1F('h', '', 7, array('d', [0, 8, 10, 14, 18, 30, 60, 200]))
print 'Opening file.'
f = ROOT.TFile.Open(filename)
print 'Making TTree.'
t = ROOT.xAOD.MakeTransientTree(f, 'CollectionTree')
print('Beginning event loop.')
n = t.GetEntries()
for e in range(n):
    if (e%(n//10))==0:
        print 'Processing event', e, '/', n
    t.GetEntry(e)
    for p in t.TruthParticles:
        pdg, aPdg = p.pdgId(), p.absPdgId()
        if aPdg==443 and checkChain(p):
            h.Fill(p.pt() * 1e-3)
            continue

ROOT.xAOD.ClearTransientTrees()
f.Close()
outputFile.Write()
outputFile.Close()