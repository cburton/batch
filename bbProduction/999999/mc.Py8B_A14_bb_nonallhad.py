# Evgen parameters
evgenConfig.description = 'bb production with single lepton filter'
evgenConfig.keywords += [ 'SM', 'QCD', 'bbbar', 'lepton']
evgenConfig.contact = ['C. D. Burton <burton@utexas.edu>']

# Run Pythia
include('Pythia8B_i/Pythia8B_A14_NNPDF23LO_Common.py')
genSeq.Pythia8B.Commands += ['HardQCD:hardbbbar = on']
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 1']

# genSeq.Pythia8B.QuarkPtCut = 0.0
# genSeq.Pythia8B.AntiQuarkPtCut = 0.0
# genSeq.Pythia8B.QuarkEtaCut = 99
# genSeq.Pythia8B.AntiQuarkEtaCut = 99
# genSeq.Pythia8B.RequireBothQuarksPassCuts = True

include("GeneratorFilters/LeptonFilter.py")
filtSeq.LeptonFilter.Ptcut = 24e3
filtSeq.LeptonFilter.Etacut = 2.6