#!/usr/bin/bash
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet

source $AtlasSetup/scripts/asetup.sh AthGeneration,21.6,latest
Gen_tf.py \
--ecmEnergy=13000 \
--jobConfig=./999999/ \
--maxEvents=100 \
--outputEVNTFile=EVNT.pool.root \
--randomSeed=$1 \

source $AtlasSetup/scripts/asetup.sh AthDerivation,21.2,latest
Reco_tf.py \
--inputEVNTFile=EVNT.pool.root \
--outputDAODFile=pool.root \
--reductionConf=TRUTH0 \
