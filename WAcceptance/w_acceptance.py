import os, sys, array
import ROOT
import numpy as np
ROOT.xAOD.Init().isSuccess()

from w_acceptance_inputs import *
filename = files_801130[int(sys.argv[1])] # DPS 63
# filename = files_500797[int(sys.argv[1])] # MOS 37
# filename = files_500796[int(sys.argv[1])] # MOO 30
# filename = files_800289[int(sys.argv[1])] # HOS 30
# filename = files_800288[int(sys.argv[1])] # HOO 30

def inAncestry(particle, ancestorPDG, allowAntiParticle=True, withStatus=0):
    nP = particle.nParents()
    if nP == 0: return 0.
    if allowAntiParticle: ancestorPDG = abs(ancestorPDG)
    for parent_index in range(nP):
        parent = particle.parent(parent_index)
        pPDG = parent.absPdgId() if allowAntiParticle else parent.pdgId()
        status = (not withStatus) or (parent.status()==withStatus)
        if pPDG == ancestorPDG and status:
            return parent.pt()
        else:
            parent_pt = inAncestry(parent, ancestorPDG, allowAntiParticle, withStatus)
            if parent_pt>0.: 
                return parent_pt
    return 0.

def checkStatus(particle, stati=[1]):
    return particle.status() in stati

def inParents(particle, parentPDG, allowAntiParticle=True):
    nP = particle.nParents()
    for parent_index in range(nP):
        if allowAntiParticle:
            pPDG = particle.parent(parent_index).absPdgId()
            parentPDG = abs(parentPDG)
        else:
            pPDG = particle.parent(parent_index).pdgId()
        if pPDG == parentPDG:
            return True
    return False

def inChildren(particle, childPDG, allowAntiParticle=True):
    nC = particle.nChildren()
    for child_index in range(nC):
        try:
            if allowAntiParticle:
                cPDG = particle.child(child_index).absPdgId()
                childPDG = abs(childPDG)
            else:
                cPDG = particle.child(child_index).pdgId()
            if cPDG == childPDG:
                return True
        except ReferenceError as e:
            pass
    return False

def checkChain(particle, pt=0., first=False, last=True, allowAntiParticle=False):
    # only take the first particle in a chain
    if first and inParents(particle, particle.pdgId(), allowAntiParticle):
        return False
    # only take the last partice in a chain
    if last and inChildren(particle, particle.pdgId(), allowAntiParticle):
        return False
    # check pT requirement if desired
    if particle.pt() < pt:
        return False
    return True

outputFile = ROOT.TFile.Open('output.root', 'recreate')

# W transverse momentum (fixed 5 GeV wide bins)
h_wPt_fixed = ROOT.TH1F('wPt_fixed', '', 40, 0, 200)

# W transverse momentum (custom bin edges)
ptbins20 = array.array('d', [0,1,2,3,4,5,6,7,8,9,10,14,18,22,26,30,64,98,132,166,200])
h_wPt_variable = ROOT.TH1F('wPt_variable', '', 20, ptbins20)

# corrleation
h_correlation = ROOT.TH2F('correlation', '', 40, 0, 200, 40, 0, 200)

print(filename)
f = ROOT.TFile.Open(filename)
t = ROOT.xAOD.MakeTransientTree(f, 'CollectionTree')
for e in range(t.GetEntries()):
    t.GetEntry(e)
    wPt, lPt, nuPt, jpsiPt = 0, 0, 0, 0
    for p in t.TruthParticles:
        pdg, aPdg = p.pdgId(), p.absPdgId()
        if not checkChain(p): # only want the last particle in a chain X->...->X
            continue
        if aPdg==24 and checkStatus(p, [62]):
            wPt = p.pt() * 1e-3
        elif aPdg==443 and checkStatus(p, [2]):
            jpsiPt = p.pt() * 1e-3
    if wPt>0:
        h_wPt_fixed.Fill(wPt)
        h_wPt_variable.Fill(wPt)
        if jpsiPt>0:
            h_correlation.Fill(wPt, jpsiPt)

ROOT.xAOD.ClearTransientTrees()
f.Close()
outputFile.Write()
outputFile.Close()