#!/usr/bin/bash
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet
source $AtlasSetup/scripts/asetup.sh AthDerivation,21.2,latest
python w_acceptance.py $1