#!/usr/bin/env python
import os
import sys
import xml.etree.ElementTree as ET

_DEBUG = False
_LOGDIR = 'log/'
_OUTDIR = '/data/cburton/ntuples/'

_NFILESPERJOB = 1
_MEMORY = None

def partition(lst, n):
    if len(lst)==0:
        print('Cannot partition an empty list.')
        return []
    if n==0:
        n = 1
    division = len(lst)/float(n)
    return [lst[int(round(division*i)):int(round(division*(i+1)))] for i in range(n)]

def buildSubmit(inputs, outdir, config):
    with open('submit', 'w') as f:
        f.write('executable = executable.sh \n')
        f.write('arguments = %s $(Item) \n' % config)
        f.write('universe = vanilla \n')
        f.write('transfer_input_files = run/ \n')
        f.write('transfer_output_files = output.root \n')
        f.write('transfer_output_remaps = "output.root = %s/output.$(Cluster)._$(Process).root" \n' % outdir)
        f.write('output = log/$(Cluster).$(Process).out \n')
        f.write('error = log/$(Cluster).$(Process).err \n')
        f.write('log = log/$(Cluster).$(Process).log \n')
        f.write('use_x509userproxy = true \n')
        f.write('x509userproxy = /home/cburton/x509proxy \n')
        if _MEMORY is not None:
            f.write('Request_Memory = %s \n' % _MEMORY)
        # f.write('nice_user = true \n')
        f.write('queue from (\n%s\n) \n' % inputs)
    os.system('chmod 755 submit')

def submitProfile(dsid, profile):
    filenames = [_.text for _ in profile]
    nJobs = len(filenames) // _NFILESPERJOB
    filenames = partition(filenames, nJobs)
    if len(filenames)>0:
        filenames = [' '.join(_) for _ in filenames]
        inputString = '\n'.join(filenames)
        name = profile.get('name')
        tag = profile.get('tag')

        outdir = _OUTDIR + '/' + dsid + '_' + tag
        os.system('mkdir -p %s' % outdir)

        config = 'generic_config_mc16' + tag + '.txt'
        # config = 'topq1_config_mc16' + tag + '.txt'

        buildSubmit(inputString, outdir, config)

        if not _DEBUG:
            os.system('condor_submit submit')
            os.system('rm submit')
        else:
            print('DEBUG: Submitting dsid=%s, profile=%s' % (dsid, name))
    else:
        print('No files for dsid=%s, profile=%s' % (dsid, profile.get('name')))


def submitDataset(dataset):
    dsid = dataset.get('dsid')
    for profile in dataset:
        submitProfile(dsid, profile)

def submitMC(mc):
    for dataset in mc:
        submitDataset(dataset)

def main():
    # Parse the input arguments
    assert len(sys.argv) >= 3, '\nCorrect usage is:\n\tpython submitJob.py [INPUTSXML] [TREE]'
    # First argument is the config file
    config = sys.argv[1]

    # All other arguments are datasets, pileup profiles, or files
    doMc = False
    mcList = []
    doData = False
    for arg in sys.argv[2:]:
        argSplit = tuple(arg.split('/'))
        if argSplit[0]=='mc':
            doMc = True
            mcList.append(argSplit[1:])
        elif argSplit[0]=='data':
            doData = True
        else:
            raise ValueError('Unknown dataset type %s from %s' % (argSplit[0], arg))

    # setup the area from/to which to send/recieve inputs/outputs
    os.system('mkdir -p %s' % _LOGDIR) # make log directory
    os.system('mkdir -p %s' % _OUTDIR) # make output directory

    # Create the XML trees
    tree = ET.parse(config)
    root = tree.getroot()
    mc = root.find('mc')
    data = root.find('data')

    # Submission of MC
    if doMc:
        # Submit requested MC
        for mcItem in mcList:
            # Submit enture mc set
            if len(mcItem)==0:
                submitMC(mc)

            # Submit a single dataset (e.g. 300005)
            if len(mcItem)==1:
                dataset = mc.find('./dataset[@dsid="%s"]' % mcItem)
                submitDataset(dataset)

            # Submit a single dataset's pileup profile group (e.g. 300005-mc16a)
            elif len(mcItem)==2:
                profile = mc.find('./dataset[@dsid="%s"]/profile[@name="%s"]' % mcItem)
                submitProfile(mcItem[0], profile)

    if doData:
        print('Running on data not supported.')

    print('Done.')

if __name__=='__main__':
    main()