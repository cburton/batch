#!/usr/bin/env bash

hostname
echo $@
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet

# Step 1: setup
source $AtlasSetup/scripts/asetup.sh AnalysisBase,21.2.197
source ${ATLAS_LOCAL_ROOT_BASE}/utilities/oldAliasSetup.sh rucio
tar xzf top-xaod.tar.gz
source usr/*/*/*/*/setup.sh
source usr/*/*/*/*/env_setup.sh

# Step 2: run MLO ntupler
python ntupler.py $@