#!/usr/bin/env python
from __future__ import print_function
import six
import subprocess

_DEBUG = False

def run(cmd, *args, **kwargs):
	'''Runs the specified command. Returns True (False) if process succeeds (fails).'''
	if _DEBUG:
		print(cmd)
	else:
		try:
			if six.PY2:
				subprocess.check_call(cmd, *args, **kwargs)
			else:
				subprocess.run(cmd, check=True, *args, **kwargs)
		except subprocess.CalledProcessError as e:
			return False
		except OSError as e:
			return False
	return True

def clean():
	'''Cleans all root files from the current directory and any subdirectories'''
	from glob import glob
	for file in glob('*.root') + glob('*/*.root'):
		print('Removing file:', file)
		run(['rm', file])