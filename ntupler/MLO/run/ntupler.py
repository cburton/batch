#!/usr/bin/env python
from __future__ import print_function
import sys
from helper import run, clean

_XCACHE = 'root://192.170.240.18:1094//'

def makeInputFile(files):
	with open('input.txt', 'w') as f:
	    f.write('\n'.join(files))

def runMlo(config):
	return run(['top-xaod', config, 'input.txt'])

def runXrootd(config, files, xcache=False):
	if xcache:
		files = [_XCACHE + _ for _ in files]
	makeInputFile(files)
	return runMlo(config)

def runXrdcp(config, files, xcache=False):
	clean()
	if xcache:
		files = [_XCACHE + _ for _ in files]
	success = True
	for file in files:
		success = success and run(['xrdcp', file, '.'])
	local_files = [_.split('/')[-1] for _ in files]
	makeInputFile(local_files)
	success = success and runMlo(config)
	return success

def runRucio(config, files):
	clean()
	files = [_.split('/')[-1] for _ in files]
	files = ['mc16_13TeV:' + _ for _ in files]
	success = run('rucio get ' + ' '.join(files), shell=True)
	makeInputFile(['mc16_13TeV/' + _ for _ in files])
	success = success and runMlo(config)
	return success

if __name__ == '__main__':
	config = sys.argv[1]
	files = sys.argv[2:]

	success = False
	# if not success: print('Attempt 1: xcache access')
	# success = success or runXrootd(config, files, xcache=True)
	if not success: print('Attempt 2: xrootd access')
	success = success or runXrootd(config, files)
	if not success: print('Attempt 3: xcache xrdcp')
	success = success or runXrdcp(config, files, xcache=True)
	if not success: print('Attempt 4: xrootd xrdcp')
	success = success or runXrdcp(config, files)
	if not success: print('Attempt 5: rucio')
	success = success or runRucio(config, files)
	if not success: print('Attempt 5b: rucio (retry)')
	success = success or runRucio(config, files)
	assert success, 'Failed to run.'