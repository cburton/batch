#!/usr/bin/env python
import subprocess
from glob import glob

_DEBUG = False
_DATADIR = '/data/cburton/ntuples/ABCD/'

dids = [
    # 300203,
    # 361100,
    # 361101,
    # 361102,
    # 361103,
    # 361104,
    # 361105,
    # 361106,
    # 361107,
    # 361108,
    # 410470,
    # 801130,
    # 'data',
]

input_sections = ['NOMINAL', 'ABCD-B', 'ABCD-C', 'ABCD-D', 'ABCD-E', 'ABCD-F']
output_sections = ['ABCD-A', 'ABCD-B', 'ABCD-C', 'ABCD-D', 'ABCD-E', 'ABCD-F']
mc_profiles = ['_a', '_d', '_e']

for did in dids:
    for input_section, output_section in zip(input_sections, output_sections):
        profiles = [''] if did=='data' else mc_profiles
        for profile in profiles:
            cmd = ['hadd', f'{_DATADIR}/{output_section}.{did}{profile}.root']
            [cmd.append(_) for _ in glob(f'{_DATADIR}/{did}{profile}/{input_section}/*.root')]
            if _DEBUG:
                print(cmd)
            else:
                subprocess.run(cmd)