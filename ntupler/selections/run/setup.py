#!/usr/bin/env python
from __future__ import print_function
import sys
from helper import run, clean

_XCACHE = 'root://192.170.240.18:1094//'

def merge(files, remote):
	if len(files) > 1 or remote:
		success = run(['hadd', 'output.root'] + files)
	else:
		success = run(['mv', files[0], 'output.root'])
	if not success:
		print('Could not merge/move files into "output.root". Files were:')
		[print('    ' + _) for _ in files]
	return success

def runLocal(files):
	if len(files) > 1:
		success = run(['hadd', 'output.root'] + files)
	else:
		success = run(['cp', files[0], 'output.root'])
	if not success:
		print('Could not merge/move files into "output.root". Files were:')
		[print('    ' + _) for _ in files]
	return success

def runHadd(files, xcache=False):
	clean()
	if xcache:
		files = [_XCACHE + _ for _ in files]
	return merge(files, remote=True)

def runXrdcp(files, xcache=False):
	clean()
	if xcache:
		files = [_XCACHE+_ for _ in files]
	for file in files:
		success = run(['xrdcp', file, '.'])
		if not success:
			return False
	local_files = [_.split('/')[-1] for _ in files]
	return merge(local_files, remote=False)

def runRucio(files):
	clean()
	files = [_.split('/')[-1] for _ in files]
	files = [_.split(':')[-1] if ':' in _ else _ for _ in files]
	success = run('rucio get ' + ' '.join(files), shell=True)
	if not success:
		return False
	return merge(['user.cburton/' + _ for _ in files], remote=False)

if __name__ == '__main__':
	files = sys.argv[1:]
	isLocal = files[0][:6]=='/data/'

	success = False
	if isLocal:
		print('Attempt 1: local access')
		success = runLocal(files)
	else:
		if not success: print('Attempt 1: xcache access')
		success = success or runHadd(files, xcache=True)
		if not success: print('Attempt 2: xrootd access')
		success = success or runHadd(files)
		if not success: print('Attempt 3: xcache xrdcp')
		success = success or runXrdcp(files, xcache=True)
		if not success: print('Attempt 4: xrootd xrdcp')
		success = success or runXrdcp(files)
		if not success: print('Attempt 5a: rucio')
		success = success or runRucio(files)
		if not success: print('Attempt 5b: rucio (retry)')
		success = success or runRucio(files)
	assert success, 'Failed to load files.'