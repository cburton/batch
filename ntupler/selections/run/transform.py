#!/usr/bin/env python
import sys
from helper import run

def transform(isData, isSlim, section):
	cmd = ['python3', 'transformFile.py', '--file', 'output.root']
	cmd.extend(['--section', section])
	if isData:
		cmd.append('--data')
	if isSlim:
		cmd.append('--slim')
	return run(cmd)

if __name__=='__main__':
	isData = sys.argv[1]=='True'
	isSlim = sys.argv[2]=='True'
	section = sys.argv[3]
	print('Transforming file.')
	success = transform(isData, isSlim, section)
	assert success, 'Failed to transform file.'