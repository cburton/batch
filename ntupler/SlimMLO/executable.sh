#!/usr/bin/env bash

# 1) SETUP
hostname
echo $@
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase;
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet;

# 2) DOWNLOAD
source ${ATLAS_LOCAL_ROOT_BASE}/utilities/oldAliasSetup.sh rucio
rucio get $1
ls mc16_13TeV/*root* > input.txt

# 3) RUN
source $AtlasSetup/scripts/asetup.sh AnalysisBase,21.2.197 # must be _after_ rucio
tar xzf top-xaod.tar.gz
source usr/*/*/*/*/setup.sh
source usr/*/*/*/*/env_setup.sh
top-xaod topq1_config_mc16a.txt input.txt
# top-xaod topq1_config_mc16d.txt input.txt
# top-xaod topq1_config_mc16e.txt input.txt