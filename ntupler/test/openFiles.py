import ROOT
import sys
inputsFile = sys.argv[1]
with open(inputsFile, 'r') as f:
	inputsList = f.readlines()
inputsList = [_.strip() for _ in inputsList]
inputsList = [_ for _ in inputsList if len(_)>0 and _[0]!='#']
for inputFile in inputsList:
	print('Opening %s' % inputFile)
	f = ROOT.TFile.Open(inputFile)
	f.Close()
print('Done')