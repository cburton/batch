#!/bin/bash
Process=$1
Extra=0
x=$(($Process+$Extra+1))
xpad=$(printf "%06d" $x)
hostname
tar xzf madevent.tar.gz
echo "Changing seed to "$xpad
sed -i -e "s/ .*iseed/    $x      = iseed/g" Cards/run_card.dat
singularity exec madgraph4_latest.sif ./bin/generate_events 2 4 run${xpad}
gunzip -c Events/run${xpad}_unweighted_events.lhe.gz > LHE.MadOnia.ColorSinglet.${xpad}.events
tar czf  LHE.MadOnia.ColorSinglet.${xpad}.tar.gz LHE.MadOnia.ColorSinglet.${xpad}.events
xrdcp -f LHE.MadOnia.ColorSinglet.${xpad}.tar.gz root://utatlas.its.utexas.edu://data_ceph/cburton/MCProduction/SPS/TAR/MadOnia/
echo "Done"
