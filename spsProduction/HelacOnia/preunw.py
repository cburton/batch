#!/usr/bin/env python
import sys

JOBID = int(sys.argv[1])
SEED = int(sys.argv[2]) + 2

PREUNW = 50000
NMC = 10000000

# CMASS = 1.5 # default
CMASS = 1.54846 # color singlet
# CMASS = 1.64846 # color octet

PROCESSES = [
    # color singlet
    'u~ d > cc~(3S11) w-',
    'd u~ > cc~(3S11) w-',
    'u d~ > cc~(3S11) w+',
    'd~ u > cc~(3S11) w+',
    'c~ s > cc~(3S11) w-',
    's c~ > cc~(3S11) w-',
    'c s~ > cc~(3S11) w+',
    's~ c > cc~(3S11) w+',

    # # color octet
    # 'u~ d > cc~(3S18) w-',
    # 'd u~ > cc~(3S18) w-',
    # 'u d~ > cc~(3S18) w+',
    # 'd~ u > cc~(3S18) w+',
    # 'c~ s > cc~(3S18) w-',
    # 's c~ > cc~(3S18) w-',
    # 'c s~ > cc~(3S18) w+',
    # 's~ c > cc~(3S18) w+',
]

# text to catenate into ho_cluster program
text = '''set seed = {seed}
set nmc = {nmc}
set preunw = {preunw}
set unwevt = {unwevt}
set nopt = {nopt}
set nopt_step = {nopt_step}
set noptlim = {noptlim}
set cmass = {cmass}
generate {process}
launch
'''

process = PROCESSES[int(JOBID % len(PROCESSES))]

parameters = {
    'seed': SEED,
    'nmc': NMC,
    'preunw': PREUNW,
    'unwevt': NMC,
    'nopt': int(NMC/10),
    'nopt_step': int(NMC/10),
    'noptlim': NMC,
    'process': process,
    'cmass': CMASS,
}

with open('process.txt', 'w') as processFile:
    processFile.write(text.format(**parameters))