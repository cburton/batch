#!/bin/bash
Process=$1
Extra=0
x=$(($Process+$Extra+1))
xpad=$(printf "%06d" $x)
echo "=========== host ==========="
hostname
more /etc/os-release
echo "========== unpack =========="
tar xzf HELAC-Onia-2.5.5.tar.gz
cd HELAC-Onia-2.5.5
echo "=========== loop ==========="
for i in {0..7}
do
    echo "    ======== python ========"
    seed=$(($x*8+$i))
    python ../preunw.py $i $seed
    more process.txt
    echo "    ======= generate ======="
    cat process.txt | ./cluster/bin/ho_cluster
done
echo "=========== copy ==========="
cp PROC_HO_*/P0_calc_0/output/*.lhe .
tar czf LHE.HelacOnia.ColorSinglet.${xpad}.tar.gz *.lhe
# tar czf LHE.HelacOnia.ColorOctet.${xpad}.tar.gz *.lhe
ls
xrdcp -f LHE.HelacOnia.ColorSinglet.${xpad}.tar.gz root://utatlas.its.utexas.edu://data_ceph/cburton/MCProduction/SPS/TAR/HelacOnia/
# xrdcp -f LHE.HelacOnia.ColorOctet.${xpad}.tar.gz root://utatlas.its.utexas.edu://data_ceph/cburton/MCProduction/SPS/TAR/HelacOnia/
echo "=========== done ==========="
