# Evgen parameters
evgenConfig.description = 'Production of J/psi using Pythia'
evgenConfig.keywords += ['Jpsi', 'QCD', 'Charmonium']
evgenConfig.contact = ['C. D. Burton <burton@utexas.edu>']

# Running Pythia
include('Pythia8B_i/Pythia8B_A14_NNPDF23LO_Common.py')
include('Pythia8B_i/Pythia8B_Charmonium_Common.py')

# Choose decay mode for J/psi
genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:2:onMode = on']

# Apply filter
include('GeneratorFilters/ParticleFilter.py')
filtSeq.ParticleFilter.Ptcut = 10000
filtSeq.ParticleFilter.Etacut = 2
filtSeq.ParticleFilter.Energycut = 1e9
filtSeq.ParticleFilter.PDG = 443
filtSeq.ParticleFilter.StatusReq = 2
filtSeq.ParticleFilter.MinParts = 1
filtSeq.ParticleFilter.Exclusive = False