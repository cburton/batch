import ROOT
ROOT.xAOD.Init().isSuccess()
import array

outputFile = ROOT.TFile.Open('output.root', 'recreate')

h = ROOT.TH2F('Jpsi', r';p_{T}^{J/#psi};|y^{J/#psi}|', 25, 0, 25, 25, 0, 2)
ptbins_8TeV = array.array('d', [10,10.5,11,11.5,12,12.5,13,14,15,16,17,18,20,22,24,26,30,35,40,60,110])
ybins_8TeV = array.array('d', [0,1,2])
h_8TeV = ROOT.TH2F('Jpsi_8TeV', r';p_{T}^{J/#psi};|y^{J/#psi}|', 20, ptbins_8TeV, 2, ybins_8TeV)

f = ROOT.TFile.Open('DAOD_TRUTH0.pool.root')
t = ROOT.xAOD.MakeTransientTree(f, 'CollectionTree')
for e in range(t.GetEntries()):
    t.GetEntry(e)
    for p in t.TruthParticles:
        if p.pdgId()==443 and p.status()==2 and p.pt()>10000 and abs(p.rapidity())<2:
            h.Fill(p.pt() * 1e-3, abs(p.rapidity()))
            h_8TeV.Fill(p.pt() * 1e-3, abs(p.rapidity()))
            break
ROOT.xAOD.ClearTransientTrees()
f.Close()

outputFile.Write()
outputFile.Close()