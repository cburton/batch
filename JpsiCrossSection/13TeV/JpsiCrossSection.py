import ROOT
ROOT.xAOD.Init().isSuccess()
import array

outputFile = ROOT.TFile.Open('output.root', 'recreate')

h = ROOT.TH2F('Jpsi', r';p_{T}^{J/#psi};|y^{J/#psi}|', 25, 0, 25, 25, 0, 2)
ptbins_13TeV = array.array('d', [8,9,10,11,12,13,14,15,16,17,18,19,20,22,24,26,28,30,35,40,45,50,55,60,70,80,90,100,120,140,160,180,200,240,300,360])
ybins_13TeV = array.array('d', [0,0.75,1.5,2])
h_13TeV = ROOT.TH2F('Jpsi_13TeV', r';p_{T}^{J/#psi};|y^{J/#psi}|', 35, ptbins_13TeV, 3, ybins_13TeV)

f = ROOT.TFile.Open('DAOD_TRUTH0.pool.root')
t = ROOT.xAOD.MakeTransientTree(f, 'CollectionTree')
for e in range(t.GetEntries()):
    t.GetEntry(e)
    for p in t.TruthParticles:
        if p.pdgId()==443 and p.status()==2 and p.pt()>8000 and abs(p.rapidity())<2:
            h.Fill(p.pt() * 1e-3, abs(p.rapidity()))
            h_13TeV.Fill(p.pt() * 1e-3, abs(p.rapidity()))
            break
ROOT.xAOD.ClearTransientTrees()
f.Close()

outputFile.Write()
outputFile.Close()