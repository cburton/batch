#!/bin/bash
# Name: executable
# Author: C. D. Burton
# Date: 27 November 2018
# Purpose: Produce Powheg+Pythia samples without EvtGen closed decays

# input arguments
Cluster=$1
Process=$2
Sample=$3
DSID=$4
Random=$((Sample+944))

hostname
echo $UID

# setup environment and MCProd
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase;
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet;
source $AtlasSetup/scripts/asetup.sh MCProd,19.2.5.33.3,notest --platform x86_64-slc6-gcc47-opt --makeflags="$MAKEFLAGS";

JO_ARRAY=(MC15JobOptions/MC15.411125.PhH7EG_H7UE_tt_mtop169p00_nonallhad_AntiB2Jpsimumu.py MC15JobOptions/MC15.411126.PhH7EG_H7UE_tt_mtop169p00_nonallhad_B2Jpsimumu.py MC15JobOptions/MC15.411127.PhH7EG_H7UE_tt_mtop171p00_nonallhad_AntiB2Jpsimumu.py MC15JobOptions/MC15.411128.PhH7EG_H7UE_tt_mtop171p00_nonallhad_B2Jpsimumu.py MC15JobOptions/MC15.411129.PhH7EG_H7UE_tt_mtop172p00_nonallhad_AntiB2Jpsimumu.py MC15JobOptions/MC15.411130.PhH7EG_H7UE_tt_mtop172p00_nonallhad_B2Jpsimumu.py MC15JobOptions/MC15.411131.PhH7EG_H7UE_tt_mtop172p25_nonallhad_AntiB2Jpsimumu.py MC15JobOptions/MC15.411132.PhH7EG_H7UE_tt_mtop172p25_nonallhad_B2Jpsimumu.py MC15JobOptions/MC15.411133.PhH7EG_H7UE_tt_mtop172p50_nonallhad_AntiB2Jpsimumu.py MC15JobOptions/MC15.411134.PhH7EG_H7UE_tt_mtop172p50_nonallhad_B2Jpsimumu.py MC15JobOptions/MC15.411135.PhH7EG_H7UE_tt_mtop172p75_nonallhad_AntiB2Jpsimumu.py MC15JobOptions/MC15.411136.PhH7EG_H7UE_tt_mtop172p75_nonallhad_B2Jpsimumu.py MC15JobOptions/MC15.411137.PhH7EG_H7UE_tt_mtop173p00_nonallhad_AntiB2Jpsimumu.py MC15JobOptions/MC15.411138.PhH7EG_H7UE_tt_mtop173p00_nonallhad_B2Jpsimumu.py MC15JobOptions/MC15.411139.PhH7EG_H7UE_tt_mtop174p00_nonallhad_AntiB2Jpsimumu.py MC15JobOptions/MC15.411140.PhH7EG_H7UE_tt_mtop174p00_nonallhad_B2Jpsimumu.py MC15JobOptions/MC15.411141.PhH7EG_H7UE_tt_mtop176p00_nonallhad_AntiB2Jpsimumu.py MC15JobOptions/MC15.411142.PhH7EG_H7UE_tt_mtop176p00_nonallhad_B2Jpsimumu.py MC15JobOptions/MC15.411143.PhPy8EG_A14_tt_mtop169p00_nonalhad_AntiB2Jpsimumu.py MC15JobOptions/MC15.411144.PhPy8EG_A14_tt_mtop169p00_nonalhad_B2Jpsimumu.py MC15JobOptions/MC15.411145.PhPy8EG_A14_tt_mtop171p00_nonalhad_AntiB2Jpsimumu.py MC15JobOptions/MC15.411146.PhPy8EG_A14_tt_mtop171p00_nonalhad_B2Jpsimumu.py MC15JobOptions/MC15.411147.PhPy8EG_A14_tt_mtop172p00_nonalhad_AntiB2Jpsimumu.py MC15JobOptions/MC15.411148.PhPy8EG_A14_tt_mtop172p00_nonalhad_B2Jpsimumu.py MC15JobOptions/MC15.411149.PhPy8EG_A14_tt_mtop172p25_nonalhad_AntiB2Jpsimumu.py MC15JobOptions/MC15.411150.PhPy8EG_A14_tt_mtop172p25_nonalhad_B2Jpsimumu.py MC15JobOptions/MC15.411151.PhPy8EG_A14_tt_mtop172p50_nonalhad_AntiB2Jpsimumu.py MC15JobOptions/MC15.411152.PhPy8EG_A14_tt_mtop172p50_nonalhad_B2Jpsimumu.py MC15JobOptions/MC15.411153.PhPy8EG_A14_tt_mtop172p75_nonalhad_AntiB2Jpsimumu.py MC15JobOptions/MC15.411154.PhPy8EG_A14_tt_mtop172p75_nonalhad_B2Jpsimumu.py MC15JobOptions/MC15.411155.PhPy8EG_A14_tt_mtop173p00_nonalhad_AntiB2Jpsimumu.py MC15JobOptions/MC15.411156.PhPy8EG_A14_tt_mtop173p00_nonalhad_B2Jpsimumu.py MC15JobOptions/MC15.411157.PhPy8EG_A14_tt_mtop174p00_nonalhad_AntiB2Jpsimumu.py MC15JobOptions/MC15.411158.PhPy8EG_A14_tt_mtop174p00_nonalhad_B2Jpsimumu.py MC15JobOptions/MC15.411159.PhPy8EG_A14_tt_mtop176p00_nonalhad_AntiB2Jpsimumu.py MC15JobOptions/MC15.411160.PhPy8EG_A14_tt_mtop176p00_nonalhad_B2Jpsimumu.py)
JO_FILE=${JO_ARRAY[$Sample]}
tar -xzvf MC15JobOptions.tar.gz

# Generate events
Generate_tf.py \
--ecmEnergy=13000 \
--jobConfig=$JO_FILE \
--maxEvents=200 \
--outputEVNTFile=output.EVNT.root \
--randomSeed=$Random \
--runNumber=$DSID

# Store the filter efficiencies
grep 'Filter Efficiency' log.generate > GenFiltEff.txt

xrdcp GenFiltEff.txt root://utatlas.its.utexas.edu://data/cburton/EVNT/GenFiltEff.$DSID.$Cluster.$Process.txt
xrdcp output.EVNT.root root://utatlas.its.utexas.edu://data/cburton/EVNT/output.EVNT.$DSID.$Cluster.$Process.root
xrdcp log.generate root://utatlas.its.utexas.edu://data/cburton/EVNT/log.$DSID.$Cluster.$Process.generate
