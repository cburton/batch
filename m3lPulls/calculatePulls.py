import sys
import pickle
import numpy as np

import ROOT
ROOT.gErrorIgnoreLevel = ROOT.kFatal

import generators

job = int(sys.argv[1])

# ==============================================================
# ====== VERSION 1: determine number of pulls from job ID ======
# if job < 10: _NPULLS = 1000
# elif job < 110: _NPULLS = 100
# else: _NPULLS = 10
# ====== VERSION 2: manually set the number of pulls ===========
_NPULLS = 25 * 5
# ==============================================================
print(f'NPULLS set to {_NPULLS}.')
# ==============================================================

_NEVENTS = 100000

# _THETAS = [0.05, 0.15, 0.25, 0.35, 0.45, 0.55, 0.65, 0.75, 0.85, 0.95]
_THETAS = np.linspace(0.05, 0.95, 25)
theta = _THETAS[job % len(_THETAS)]

ROOT.RooRandom.randomGenerator().SetSeed(job)

workspaceFile = ROOT.TFile.Open('workspace.root')
w = workspaceFile.Get('w')
workspaceFile.Close()

[var.setConstant() for var in w.allVars()]
w.var('theta').setConstant(ROOT.kFALSE)

responses, errors, pulls, results = [], [], [], []
for i in range(_NPULLS):
    # ====== VERSION 1 ======
    # testDataset = w.pdf(f'model{theta}').generate(
    #     ROOT.RooArgSet(w.var('m3l')),
    #     ROOT.RooFit.NumEvents(_NEVENTS),
    #     ROOT.RooFit.Name('testDataset'),
    # )
    # ====== VERSION 2 ======
    testData = generators.generateToyData(theta, 100000, (0, 250))
    testDataset = ROOT.RooDataSet(
        f'testDataset', f'testDataset',
        ROOT.RooArgSet(w.var('m3l'), w.var('eventWeight')),
        ROOT.RooFit.WeightVar(w.var('eventWeight'))
    )
    for testDatum in testData:
        observable = round(testDatum[1], 6)
        weight = 1.
        w.var('m3l').setVal(observable)
        w.var('eventWeight').setVal(weight)
        testDataset.add(ROOT.RooArgSet(w.var('m3l'), w.var('eventWeight')))
    # =======================

    morph = w.pdf('morph')
    fit = morph.fitTo(testDataset, ROOT.RooFit.Save(), ROOT.RooFit.PrintLevel(-1))

    response = w.var('theta').getValV()
    error = w.var('theta').getError()
    responses.append(response)
    errors.append(error)
    pulls.append((response - theta) / error)
    results.append(fit.status())

    # w.var('theta').setVal(fit.randomizePars()[0].getValV())
    # w.var('theta').setVal(0.5)

with open(f'output_{job}.pickle', 'wb') as f:
    pickle.dump(theta, f)
    pickle.dump(responses, f)
    pickle.dump(errors, f)
    pickle.dump(pulls, f)
    pickle.dump(results, f)
