import numpy as np
from random import shuffle as lshuffle

def generateToyData(parameters, numpoints, obsrange):
    observable = M3L(f=0.45, mean=[10, 69], sigma=19.5, alpha=2.1, theta=42.4, mu=10)
    generated = observable.generate(parameters, numpoints, rtype='list')
    generated = [_ for _ in generated if obsrange[0] < _[1] < obsrange[1]]
    return generated

def convert(array, rtype, **kwargs):
    '''Converts array to the desired type'''
    if isinstance(array, list):
        if rtype=='list':
            return array
        elif rtype=='numpy':
            return np.array(array)
        elif rtype=='tensor':
            return numpy2torch(array, **kwargs)
    elif isinstance(array, np.ndarray):
        if rtype=='list':
            return array.tolist()
        elif rtype=='numpy':
            return array
        elif rtype=='tensor':
            return numpy2torch(array, **kwargs)
    elif isinstance(array, torch.Tensor):
        if rtype=='list':
            return array.detach().numpy().tolist()
        elif rtype=='numpy':
            return array.detach().numpy()
        elif rtype=='tensor':
            return array
    else:
        print('Unknown array type: {}'.format(type(array)))

class Observable:
    '''docstring for Observable'''
    def __init__(self,  **kwargs):
        self.kwargs = kwargs
        # functions that map parameters to observables' fit functions' coefficients
        self.converters = []
        for coefficient, value in self.coefficients.items():
            self.converters.append(getattr(self, 'to_'+coefficient))
            if coefficient in kwargs:
                value = kwargs[coefficient]
            setattr(self, coefficient, value)

    def generate(self, parameters, nTraining, rtype='tensor', shuffle=True, truth=True):
        # convert parameters to a numpy array
        if not hasattr(parameters, '__len__'):
            parameters = [parameters]
        parameters = np.array(parameters)
        # generate 1/nParameters of the data at each parameter
        nTrainingPerParameter = int(nTraining/len(parameters))

        # for each true parameter value, get the coefficient for the observable's shape
        # e.g. Gaussian::to_mean() gives a function mu(true) = (true * 6) - 3
        coefficients = []
        for converter in self.converters:
            coefficients.append(converter(parameters))
        coefficients = np.array(coefficients).T

        # generate samples of the observable's shape and put them into an 2D list
        allParameterData = []
        for parameter, coefficient in zip(parameters, coefficients):
            singleParameterData = self.generator(*tuple(coefficient), nTrainingPerParameter)
            for dataPoint in singleParameterData:
                # keep the (parameter, observable) pair if desired
                dataToAppend = [parameter, dataPoint] if truth else [dataPoint]
                allParameterData.append(dataToAppend)

        if shuffle: # lshuffle is python's random.shuffle
            lshuffle(allParameterData)

        return convert(allParameterData, rtype)

class M3L(Observable):
    def __init__(self, **kwargs):
        self.coefficients = {
            'f': 0.5,
            'mean': [10, 70], 'sigma': 20, # Gaussian parameters
            'alpha': 2.5, 'theta': 33, 'mu': 9 # Gamma parameters
        }
        super(M3L, self).__init__(**kwargs)

    def generator(self, f, mean, sigma, alpha, theta, mu, n):
        nGauss, nGamma = int(n * f), int(n * (1 - f))
        gauss = np.random.normal(loc=mean, scale=sigma, size=nGauss)
        gamma = np.random.gamma(shape=alpha, scale=theta, size=nGamma) + mu
        m3l = np.concatenate([gauss, gamma])
        return m3l

    def to_f(self, truth):
        return self.f * np.ones(len(truth))

    def to_mean(self, truth):
        return self.mean[0] * truth + self.mean[1]

    def to_sigma(self, truth):
        return self.sigma * np.ones(len(truth))

    def to_alpha(self, truth):
        return self.alpha * np.ones(len(truth))

    def to_theta(self, truth):
        return self.theta * np.ones(len(truth))

    def to_mu(self, truth):
        return self.mu * np.ones(len(truth))
