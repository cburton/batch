#!/usr/bin/env python
import subprocess
import re
import sys
from triggerLists import *

job = int(sys.argv[1])

grlBase = '/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/'
grlList = [
    'data15_13TeV/20170619/physics_25ns_21.0.19.xml',
    'data16_13TeV/20180129/physics_25ns_21.0.19.xml',
    'data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.xml',
    'data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.xml'
]

# triggerList = triggers15 # 28, 112
# triggerList = triggers16 # 57, 228
# triggerList = triggers17 # 
# triggerList = triggers18 # 
triggerList = triggersRun2 # 90, 360

grl = grlBase + grlList[job // len(triggerList)]
# grl = 'testGRL.xml'
trigger, seed = triggerList[job % len(triggerList)]

iLumiCalc = [
    'iLumiCalc',
    '--lumitag=OflLumi-13TeV-010',
    '--livetrigger=%s' % seed,
    '--trigger=%s' % trigger,
    '--xml=%s' % grl,
    '--lar',
    '--lartag=LARBadChannelsOflEventVeto-RUN2-UPD4-10',
]

# print(' '.join(iLumiCalc))
# sys.exit(0)

process = subprocess.Popen(iLumiCalc, stdout=subprocess.PIPE)
output, error = process.communicate()
output = output.splitlines()

matchrun = re.compile(r'Run ([0-9]+) LB \[([0-9]+)-([0-9]+)\]')
matchlumidel = re.compile(r': IntL delivered \(ub\^-1\) : ([0-9\.e+]+)')
matchlumipre = re.compile(r': IntL after livefraction \(ub\^-1\):  ([0-9\.e+-]+)')
matchlumilar = re.compile(r': IntL after LAr fraction \(ub\^-1\):  ([0-9\.e+-]+)')
matchlumirec = re.compile(r': IntL recorded after prescale \(ub\^-1\) : ([0-9\.e+-]+)')
matchgoodlb = re.compile(r': Good LBs     : ([0-9]+)')
matchbadlb = re.compile(r': Bad LBs      : ([0-9]+)')

matchtotlumidel = re.compile(r': Total IntL delivered \(ub\^-1\) : ([0-9\.e+-]+)')
matchtotlumipre = re.compile(r': Total IntL after livefraction \(ub\^-1\):  ([0-9\.e+-]+)')
matchtotlumilar = re.compile(r': Total IntL after LAr fraction \(ub\^-1\):  ([0-9\.e+-]+)')
matchtotlumirec = re.compile(r': Total IntL recorded \(ub\^-1\) : ([0-9\.e+-]+)')
matchtotgoodlb = re.compile(r': Total Good LBs     : ([0-9]+)')
matchtotbadlb = re.compile(r': Total Bad LBs     : ([0-9]+)')

runset = set()
lumidel = dict()
lumirec = dict()
lumipre = dict()
lumilar = dict()
goodlb = dict()
badlb = dict()

for line in output:
    line = str(line)

    m = matchrun.search(line)
    if m:
        currentrun = m.group(1)
        runset.add(currentrun)

    m = matchlumidel.search(line)
    if m: lumidel[currentrun] = float(m.group(1)) + lumidel.get(currentrun, 0.)
        
    m = matchlumirec.search(line)
    if m: lumirec[currentrun] = float(m.group(1)) + lumirec.get(currentrun, 0.)

    m = matchlumipre.search(line)
    if m: lumipre[currentrun] = float(m.group(1)) + lumipre.get(currentrun, 0.)

    m = matchlumilar.search(line)
    if m: lumilar[currentrun] = float(m.group(1)) + lumilar.get(currentrun, 0.)

    m = matchgoodlb.search(line)
    if m: goodlb[currentrun] = int(m.group(1)) + goodlb.get(currentrun, 0)

    m = matchbadlb.search(line)
    if m: badlb[currentrun] = int(m.group(1)) + badlb.get(currentrun, 0)

    m = matchtotlumidel.search(line)
    if m: lumidel['Total'] = float(m.group(1)) + lumidel.get('Total', 0.)

    m = matchtotlumirec.search(line)
    if m: lumirec['Total'] = float(m.group(1)) + lumirec.get('Total', 0.)

    m = matchtotlumipre.search(line)
    if m: lumipre['Total'] = float(m.group(1)) + lumipre.get('Total', 0.)

    m = matchtotlumilar.search(line)
    if m: lumilar['Total'] = float(m.group(1)) + lumilar.get('Total', 0.)

    m = matchtotgoodlb.search(line)
    if m: goodlb['Total'] = int(m.group(1)) + goodlb.get('Total', 0)

    m = matchtotbadlb.search(line)
    if m: badlb['Total'] = int(m.group(1)) + badlb.get('Total', 0)

runlist = list(runset)
runlist.sort()
runlist.append('Total')

totalLumi = lumirec.get('Total', 0.)
if totalLumi > 1e8:
    scale = 1e-6
elif totalLumi > 1e5:
    scale = 1e-3
elif totalLumi > 1e2:
    scale = 1.
else:
    scale = 1e3

f = open('lumitable.csv', 'w')
f.write('Run, Good, Bad, LDelivered, LRecorded')
f.write(', LAr Corrected')
f.write(', Prescale Corrected, Live Fraction')
f.write(', LAr Fraction')
f.write(', Prescale Fraction\n')
for run in runlist:
    if run == 'Total':
        f.write('Total, ')
    else:
        f.write((run+', '))
        f.write('%d, %d, %f, %f, ' % (goodlb.get(run, 0), badlb.get(run, 0), scale*lumidel.get(run, 0.), scale*lumipre.get(run, 0.)))
    f.write('%f, ' % (scale*lumilar.get(run, 0.)))
    f.write('%f, ' % (scale*lumirec.get(run, 0.)))
    if lumidel.get(run, 0.) > 0.:
        f.write('%.2f, ' % (100*lumipre.get(run, 0.)/lumidel.get(run, 0.)))
    else:
        f.write(' , ')

    if lumipre.get(run, 0.) > 0.:
        f.write('%.2f, ' % (100*lumilar.get(run, 0.)/lumipre.get(run, 0.)))
    else:
        f.write(' , ')
    if lumirec.get(run, 0.) > 0.:
        f.write('%.2f' % (100*lumilar.get(run, 0.)/lumirec.get(run, 0.)))
    f.write('\n')

f.write('#' + ' '.join(iLumiCalc))
f.close()

f = open('lumisummary.txt', 'w')
f.write(trigger)
f.write('\t')
f.write('%f' % (scale*lumirec.get('Total', 0.)))
f.close()